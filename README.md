# ECE 567 Software Engineering I

## Deadlines

| Item                                                | Due Date     |
|-----------------------------------------------------|--------------|
| Proposal                                            | September 17 |
| First Report (Statement of Work & Requirements)     | September 24 |
| First Report (Functional Requirements Specs & UI)   | October 1    |
| First Report (Full)                                 | October 8    |
| Second Report (Interaction Diagrams)                | October 15   |
| Second Report (Class Diagram & System Architecture) | October 22   |
| Second Report (Full)                                | October 29   |
| First Demo                                          | November 1   |
| Third Report (All Reports Collated)                 | December 10  |
| Second Demo                                         | December 13  |
| Electronic Project Archive                          | December 16  |


## Cloud Server Installation
- Create a Python virtual environment
    * [Virutal Environment Guide](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
- Install required modules
    * `pip install -r requirements.txt`
    * `sudo apt-get install python-dev libmysqlclient-dev`
- Set environment variables in "admin_app_config.py"
- Run development server
    * `python app.py 8080`
